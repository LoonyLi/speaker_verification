import os
import models
import argparse
from dataset.librispeech import LibriSpeech
from utils.config import LIBRISPEECH_SAMPLING_RATE, PATH
from keras.callbacks import CSVLogger, ModelCheckpoint, ReduceLROnPlateau
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.layers import Dense
from utils.preprocessing import preprocess_classifier
from utils.evaluation import NShotEvaluation
from utils.classifier_batch_generator import ClassifierBatchGenerator

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

parser = argparse.ArgumentParser(description='Trains siamese network for speaker verification')
parser.add_argument('--model', default='conv_4_classifier', nargs='?',
                   help='Model name from "models" directory')

parser.add_argument('--num_seconds', type=int, default=3, nargs='?',
                   help='Number of seconds to extract from each audio sample')

parser.add_argument('--batch_size', type=int, default=32, nargs='?',
                   help='Size of the batch')

parser.add_argument('--embedding_dim', type=int, default=64, nargs='?',
                   help='Dimension of output d-vector embedding')

parser.add_argument('--num_filters', type=int, default=64, nargs='?',
                   help='Number of convolutional num_filters')

parser.add_argument('--sampling_rate', type=int, default=4, nargs='?',
                   help='Downsample audio data to this rate')

parser.add_argument('--n_shot', type=int, default=1, nargs='?',
                   help='Number of samples of each class in the support set')

parser.add_argument('--k_way', type=int, default=5, nargs='?',
                   help='Number of classes in the support set')

parser.add_argument('--training_set', nargs='+', default=['train-clean-100', 'train-clean-360'],
                   help='Subsets of Librispeech dataset to use for training')

parser.add_argument('--validation_set', default='dev-clean', nargs='?',
                   help='Subset of Librispeech dataset to use for validation')

parser.add_argument('--num_epochs', type=int, default=50, nargs='?',
                   help='Number of epochs for training')

parser.add_argument('--evaluate_every_n_batches', type=int, default=500, nargs='?',
                   help='Evaluate with n-shot k-way classification every n batches')

parser.add_argument('--num_evaluation_tasks', type=int, default=500, nargs='?',
                   help='Number of n-shot k-way classification tasks per evaluation step')

args = parser.parse_args()

model_name_to_dump = 'classifier_{}_{}_embedding_{}_num_filters'\
                     .format(args.model, args.embedding_dim, args.num_filters)

input_length = int(LIBRISPEECH_SAMPLING_RATE * args.num_seconds / args.sampling_rate)

#------Load dataset------
train_set = LibriSpeech(args.training_set, args.num_seconds)
valid_set = LibriSpeech(args.validation_set, args.num_seconds, shuffle=False)

# Map speaker IDs to the range 0 - (train.num_classes() - 1)
unique_speakers = sorted(train_set.df['speaker_id'].unique())
speaker_id_mapping = {unique_speakers[i]: i for i in range(train_set.num_classes())}
train_generator = ClassifierBatchGenerator(train_set, args.batch_size, train_set.num_classes(), speaker_id_mapping)

#------Define model------
base_model = getattr(models, args.model)(num_filters=args.num_filters,
                                         embedding_dim=args.embedding_dim,
                                         input_shape=(input_length, 1))

base_model.add(Dense(train_set.num_classes(), activation='softmax'))  # Add output classification layer

base_model.compile(loss="categorical_crossentropy", optimizer=Adam(clipnorm=1.), metrics=['accuracy'])
plot_model(base_model, to_file=os.path.join(PATH, 'classifier.png'), show_shapes=True)

#------Train model------
base_model.fit_generator(
    generator=train_generator,
    validation_steps=100,
    epochs=args.num_epochs,
    steps_per_epoch=2 * args.evaluate_every_n_batches,  # 'cause in each batch here twice as less samples as for siamese
    use_multiprocessing=False,
    callbacks=[
        NShotEvaluation(args.num_evaluation_tasks, args.n_shot, args.k_way,
                        valid_set, preprocessor=preprocess_classifier, network_type='classifier'),

        CSVLogger(os.path.join(PATH, 'logs', '{}.csv'.format(model_name_to_dump))),

        ModelCheckpoint(os.path.join(PATH, 'models', '{}.hdf5'.format(model_name_to_dump)),
                        monitor='{}-shot_acc'.format(args.n_shot), save_best_only=True, mode='max', verbose=True),

        ReduceLROnPlateau(monitor='{}-shot_acc'.format(args.n_shot), mode='max', verbose=True)
    ]
)