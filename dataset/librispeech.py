import os
import numpy as np
import pandas as pd
import soundfile as sf
from tqdm import tqdm
from keras.utils import Sequence

from utils.config import PATH, LIBRISPEECH_SAMPLING_RATE


class LibriSpeech(Sequence):
    def __init__(self, subsets, seconds, shuffle=True):
        self.subset = subsets if not isinstance(subsets, str) else [subsets]
        self.fragment_seconds = seconds
        self.fragment_length = int(seconds * LIBRISPEECH_SAMPLING_RATE)
        self.shuffle = shuffle

        cached_df = []
        found_cache = {s: False for s in self.subset}
        for s in self.subset:
            subset_index_path = os.path.join(PATH, 'data', '{}.index.csv'.format(s))
            if os.path.exists(subset_index_path):
                cached_df.append(pd.read_csv(subset_index_path))
                found_cache[s] = True

        if all(found_cache.values()):
            self.df = pd.concat(cached_df)
        else:
            df = pd.read_csv(os.path.join(PATH, 'data', 'LibriSpeech', 'SPEAKERS.TXT'),
                             skiprows=11, delimiter='|', error_bad_lines=False)
            df.columns = [col.strip().replace(';', '').lower() for col in df.columns]
            df = df.assign(subset=df['subset'].apply(lambda x: x.strip()), name=df['name'].apply(lambda x: x.strip()))

            audio_files = []
            for subset, found in found_cache.items():
                if not found:
                    audio_files += self.indexing(subset)

            df = pd.merge(df, pd.DataFrame(audio_files))
            self.df = pd.concat(cached_df+[df])

        for s in self.subset:
            self.df[self.df['subset'] == s].to_csv(os.path.join(PATH, 'data', '{}.index.csv'.format(s)), index=False)

        self.unique_speakers_number = len(self.df['id'].unique())
        self.df = self.df.rename(columns={'id': 'speaker_id', 'minutes': 'speaker_minutes'})
        self.df = self.df.reset_index(drop=True)
        self.df = self.df.assign(id=self.df.index.values)

        self.datasetid_to_filepath = self.df.to_dict()['filepath']
        self.datasetid_to_speaker_id = self.df.to_dict()['speaker_id']
        print(('Finished indexing data. {} files processed.'.format(len(self))))

    def __len__(self):
        return len(self.df)

    def num_classes(self):
        return len(self.df['speaker_id'].unique())

    def __getitem__(self, index):
        audio_sample, sampling_rate = sf.read(self.datasetid_to_filepath[index])
        # Take a random sample of the file of the desired length
        fragment_start_index = np.random.randint(0, max(len(audio_sample) - self.fragment_length, 1)) if self.shuffle else 0
        audio_sample = audio_sample[fragment_start_index:fragment_start_index + self.fragment_length]

        # If length is not enough, pad with zeros
        if len(audio_sample) < self.fragment_length:
            less_timesteps = self.fragment_length - len(audio_sample)
            if self.shuffle:
                # Append 0s to random places before/after the audio_sample
                less_timesteps = self.fragment_length - len(audio_sample)
                before_len = np.random.randint(0, less_timesteps)
                after_len = less_timesteps - before_len
                audio_sample = np.pad(audio_sample, (before_len, after_len), 'constant')
            else:
                # Append 0s to the end
                audio_sample = np.pad(audio_sample, (0, less_timesteps), 'constant')

        label = self.datasetid_to_speaker_id[index]
        return audio_sample, label

    """Returns random sample with his label"""
    def get_sample_and_label(self):
        random_sample = self.df.sample(1, weights='length')
        return self[random_sample['id'].index.values[0]], random_sample['id']

    """Returns a list of IDs pairs belonging to the same speaker"""
    def get_same_speaker_samples(self, num_pairs):
        num_pairs = int(num_pairs)
        same_speaker_samples = pd.merge(self.df.sample(num_pairs*2, weights='length'), self.df, on='speaker_id'
                                        ).sample(num_pairs)[['speaker_id', 'id_x', 'id_y']]
        same_speaker_samples = list(zip(same_speaker_samples['id_x'].values,
                                        same_speaker_samples['id_y'].values))
        return same_speaker_samples

    """Returns a list of IDs pairs belonging to different speakers"""
    def get_different_speaker_samples(self, num_pairs):
        num_pairs = int(num_pairs)
        random_sample = self.df.sample(num_pairs, weights='length')
        random_sample_from_other_speakers = self.df[~self.df['speaker_id'].isin(random_sample['speaker_id'])]\
                                                                          .sample(num_pairs, weights='length')
        differing_pairs = list(zip(random_sample['id'].values, random_sample_from_other_speakers['id'].values))

        return differing_pairs

    """Returns verification batch for siamese network"""
    def get_verification_batch(self, batch_size):
        # Half of the pairs will be from the same speaker
        same_speaker_inputs = self.get_same_speaker_samples(batch_size / 2)
        input_1_same = np.stack([self[i][0] for i in list(zip(*same_speaker_inputs))[0]])
        input_2_same = np.stack([self[i][0] for i in list(zip(*same_speaker_inputs))[1]])

        # Second half - from the different speakers
        pairs_different_speakers = self.get_different_speaker_samples(batch_size / 2)
        different_speaker_inputs = np.stack([self[i][0] for i in list(zip(*pairs_different_speakers))[0]])
        input_2_different = np.stack([self[i][0] for i in list(zip(*pairs_different_speakers))[1]])

        input_1 = np.vstack([input_1_same, different_speaker_inputs])[:, :, np.newaxis]
        input_2 = np.vstack([input_2_same, input_2_different])[:, :, np.newaxis]
        outputs = np.append(np.zeros(batch_size // 2), np.ones(batch_size // 2))[:, np.newaxis]

        return [input_1, input_2], outputs

    """Yields verification batches for siamese network forever"""
    def get_batch(self, batch_size):
        while True:
            ([input_1, input_2], labels) = self.get_verification_batch(batch_size)
            yield ([input_1, input_2], labels)

    """Returns a query sample and support set of n audio samples each from k different speakers"""
    def get_n_shot_samples(self, k, n=1):
        #if 1 <= k <= self.unique_speakers_number:
        #    raise ValueError

        query = self.df.sample(1, weights='length')
        query_sample = self[query.index.values[0]]

        not_same_sample = self.df.index != query.index.values[0]
        is_query_speaker = self.df['speaker_id'] == query['speaker_id'].values[0]
        correct_samples = self.df[is_query_speaker & not_same_sample].sample(n, weights='length')

        other_speakers = np.random.choice(self.df[~is_query_speaker]['speaker_id'].unique(), k-1, replace=False)
        other_samples = []
        for i in range(k-1):
            is_same_speaker = self.df['speaker_id'] == other_speakers[i]
            other_samples.append(self.df[~is_query_speaker & is_same_speaker].sample(n, weights='length'))

        support_set = pd.concat([correct_samples] + other_samples)
        support_set_samples = tuple(np.stack(i) for i in zip(*[self[i] for i in support_set.index]))

        return query_sample, support_set_samples

    """Processes and saves info about dataset subset data"""
    @staticmethod
    def indexing(subset):
        audio_files = []
        dataset_path = os.path.join(PATH, "data", "LibriSpeech", "{}".format(subset))
        print(('Indexing {}'.format(subset)))

        files_number = 0
        for root, folders, files in os.walk(dataset_path):
            files_number += len([f for f in files if f.endswith('.flac')])

        progress_bar = tqdm(total=files_number)
        for root, folders, files in os.walk(dataset_path):
            if len(files) == 0:
                continue
            try:
                librispeech_id = int(root.split(os.sep)[-2])                
            except Exception as e:
                print("[WARNING]: {}, skipping {} {} {}".format(e, root, folders, files))
                continue
    
            for f in files:
                if f.endswith('.flac'):
                    progress_bar.update(1)
                    audio, sampling_rate = sf.read(os.path.join(root, f))
                    audio_files.append({'id': librispeech_id, 'filepath': os.path.join(root, f), 'length': len(audio),
                                        'seconds': len(audio) * 1. / LIBRISPEECH_SAMPLING_RATE})
        progress_bar.close()
        return audio_files
