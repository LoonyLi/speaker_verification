import os
import models
import argparse
from dataset.librispeech import LibriSpeech
from utils.config import LIBRISPEECH_SAMPLING_RATE, PATH
from keras.optimizers import Adam
from keras.callbacks import CSVLogger, ModelCheckpoint, ReduceLROnPlateau
from keras.utils import plot_model
from utils.preprocessing import preprocess_siamese
from utils.evaluation import NShotEvaluation

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

parser = argparse.ArgumentParser(description='Trains siamese network for speaker verification')
parser.add_argument('--model', default='conv_4_classifier', nargs='?',
                   help='Model name from "models" directory')

parser.add_argument('--num_seconds', type=int, default=3, nargs='?',
                   help='Number of seconds to extract from each audio sample')

parser.add_argument('--batch_size', type=int, default=32, nargs='?',
                   help='Size of the batch')

parser.add_argument('--embedding_dim', type=int, default=64, nargs='?',
                   help='Dimension of output d-vector embedding')

parser.add_argument('--num_filters', type=int, default=64, nargs='?',
                   help='Number of convolutional num_filters')

parser.add_argument('--sampling_rate', type=int, default=4, nargs='?',
                   help='Downsample audio data to this rate')

parser.add_argument('--n_shot', type=int, default=1, nargs='?',
                   help='Number of samples of each class in the support set')

parser.add_argument('--k_way', type=int, default=5, nargs='?',
                   help='Number of classes in the support set')

parser.add_argument('--training_set', nargs='+', default=['train-clean-100', 'train-clean-360'],
                   help='Subsets of Librispeech dataset to use for training')

parser.add_argument('--validation_set', default='dev-clean', nargs='?',
                   help='Subset of Librispeech dataset to use for validation')

parser.add_argument('--distance_metric', default='uniform_euclidean', nargs='?',
                   help='Metric of a distance between two d-vectors')

parser.add_argument('--num_epochs', type=int, default=50, nargs='?',
                   help='Number of epochs for training')

parser.add_argument('--evaluate_every_n_batches', type=int, default=500, nargs='?',
                   help='Evaluate with n-shot k-way classification every n batches')

parser.add_argument('--num_evaluation_tasks', type=int, default=500, nargs='?',
                   help='Number of n-shot k-way classification tasks per evaluation step')

args = parser.parse_args()

model_name_to_dump = 'siamese_{}_{}_embedding_{}_num_filters'\
                     .format(args.model, args.embedding_dim, args.num_filters)

input_length = int(LIBRISPEECH_SAMPLING_RATE * args.num_seconds / args.sampling_rate)

#------Load dataset------
train_set = LibriSpeech(args.training_set, args.num_seconds)
valid_set = LibriSpeech(args.validation_set, args.num_seconds, shuffle=False)

train_generator = (preprocess_siamese(batch) for batch in train_set.get_batch(args.batch_size))
valid_generator = (preprocess_siamese(batch) for batch in train_set.get_batch(args.batch_size))

#------Define model------
base_model = getattr(models, args.model)(num_filters=args.num_filters, 
                                         embedding_dim=args.embedding_dim)

siamese_model = models.create_siamese_model(base_model=base_model, 
                                            input_shape=(input_length, 1), 
                                            distance_metric=args.distance_metric)

siamese_model.compile(loss="binary_crossentropy", optimizer=Adam(clipnorm=1.), metrics=['accuracy'])
plot_model(siamese_model, to_file=os.path.join(PATH, 'siamese.png'), show_shapes=True)

#------Train model------
siamese_model.fit_generator(
    generator=train_generator,
    validation_data=valid_generator,
    validation_steps=100,
    epochs=args.num_epochs,
    steps_per_epoch=args.evaluate_every_n_batches,
    use_multiprocessing=False,
    callbacks=[
        NShotEvaluation(args.num_evaluation_tasks, args.n_shot, args.k_way,
                        valid_set, preprocessor=preprocess_siamese),

        CSVLogger(os.path.join(PATH, 'logs', '{}.csv'.format(model_name_to_dump))),

        ModelCheckpoint(os.path.join(PATH, 'models', '{}.hdf5'.format(model_name_to_dump)),
                        monitor='{}-shot_acc'.format(args.n_shot), save_best_only=True, mode='max', verbose=True),

        ReduceLROnPlateau(monitor='{}-shot_acc'.format(args.n_shot), mode='max', verbose=True)
    ]
)