from keras.models import Model, Sequential
from keras import layers
import keras.backend as K


def create_siamese_model(base_model, input_shape, distance_metric='uniform_euclidean'):
    input_1, input_2 = layers.Input(input_shape), layers.Input(input_shape)
    embedding_1, embedding_2 = base_model(input_1), base_model(input_2)

    if distance_metric == 'weighted_l1':
        emb_distance = layers.Subtract()([embedding_1, embedding_2])
        emb_distance = layers.Lambda(lambda x: K.abs(x))(emb_distance)
        output = layers.Dense(1, activation='sigmoid')(emb_distance)

    elif distance_metric == 'uniform_euclidean':
        emb_distance = layers.Subtract(name='subtract_embeddings')([embedding_1, embedding_2])
        emb_distance = layers.Lambda(lambda x: K.sqrt(K.sum(K.square(x), axis=-1, keepdims=True)), name='euclidean_distance')(emb_distance)
        output = layers.Dense(1, activation='sigmoid')(emb_distance)
    else:
        raise NotImplementedError

    return Model(inputs=[input_1, input_2], outputs=output)


def conv_4_classifier(num_filters, embedding_dim, input_shape=None):
    model = Sequential()
    if input_shape is None:
        model.add(layers.Conv1D(num_filters, 32, activation='relu', padding='same'))
    else:
        model.add(layers.Conv1D(num_filters, 32, activation='relu', padding='same', input_shape=input_shape))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool1D(4, 4))

    model.add(layers.Conv1D(2 * num_filters, 3, activation='relu', padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool1D())

    model.add(layers.Conv1D(3 * num_filters, 3, activation='relu', padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool1D())

    model.add(layers.Conv1D(4 * num_filters, 3, activation='relu', padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool1D())

    model.add(layers.GlobalMaxPool1D())
    model.add(layers.Dense(embedding_dim))

    return model
