import numpy as np
from keras.utils import to_categorical


def preprocess_classifier(batch, num_classes, speaker_id_mapping, sampling_rate=4):
    input, labels = batch
    input = whitening(input)
    if sampling_rate:
        input = downsample(input, sampling_rate)

    labels = transform_labels(num_classes, speaker_id_mapping, labels)
    return input, labels


def preprocess_siamese(batch, sampling_rate=4):
    ([input_1, input_2], labels) = batch
    input_1 = whitening(input_1)
    input_2 = whitening(input_2)
    if sampling_rate:
        input_1 = downsample(input_1, sampling_rate)
        input_2 = downsample(input_2, sampling_rate)
    
    return [input_1, input_2], labels


def whitening(batch, rms=0.038021):
    """Transform a batch so each sample has 0 mean and the same root mean square amplitude (volume)"""
    sample_wise_mean = batch.mean(axis=1)
    whitened_batch = batch - np.tile(sample_wise_mean, (1, 1, batch.shape[1])).transpose((1, 2, 0))
    sample_wise_rescaling = rms / np.sqrt(np.power(batch, 2).mean())
    whitened_batch = whitened_batch * np.tile(sample_wise_rescaling, (1, 1, batch.shape[1])).transpose((1, 2, 0))
    return whitened_batch


def downsample(batch, sampling_rate):
    return batch[:, ::sampling_rate, :]


def transform_labels(num_classes, speaker_id_mapping, labels_vector):
    labels_vector = np.array([speaker_id_mapping[i] for i in labels_vector[:, 0]])[:, np.newaxis]
    return to_categorical(labels_vector, num_classes)

