import numpy as np
from keras.utils import Sequence
from utils.preprocessing import preprocess_classifier


class ClassifierBatchGenerator(Sequence):
    def __init__(self, sequence, batch_size, num_classes, speaker_id_mapping):
        self.sequence = sequence
        self.batch_size = batch_size
        self.underlying_indexes = list(range(len(sequence)))
        self.n_classes = num_classes,
        self.speaker_id_mapping = speaker_id_mapping
        self.on_epoch_end()

    def __len__(self):
        return int(len(self.sequence) / float(self.batch_size))

    def __getitem__(self, item):
        X, y = [], []
        for underlying_i in self.batch_to_index[item]:
            sample = self.sequence[underlying_i]
            X.append(sample[0][:, np.newaxis])
            y.append(sample[1])

        X, y = np.stack(X), np.stack(y)[:, np.newaxis]
        X, y = preprocess_classifier((X, y), self.n_classes[0], self.speaker_id_mapping)
        return X, y

    def on_epoch_end(self):
        np.random.shuffle(self.underlying_indexes)
        self.batch_to_index = {i: self.underlying_indexes[i * self.batch_size:(i + 1) * self.batch_size] for i in range(len(self))}
