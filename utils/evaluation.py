import numpy as np
from tqdm import tqdm
from keras.models import clone_model
from keras.callbacks import Callback


class NShotEvaluation(Callback):
    def __init__(self, num_tasks, n_shot, k_way, dataset, preprocessor=lambda x: x, network_type='siamese'):
        super(NShotEvaluation, self).__init__()
        self.dataset = dataset
        self.num_tasks = num_tasks
        self.n_shot = n_shot
        self.k_way = k_way
        self.preprocessor = preprocessor
        self.network_type = network_type

    def on_epoch_end(self, epoch, logs=None):
        n_correct = test_n_shot(self.model, self.dataset, self.preprocessor, self.num_tasks, self.n_shot,
                                self.k_way, network_type=self.network_type)
        n_shot_acc = n_correct * 1. / self.num_tasks
        logs = logs or {}
        logs['{}-shot_acc'.format(self.n_shot)] = n_shot_acc
        print('{}-shot_acc: {:.4f}'.format(self.n_shot, n_shot_acc))


def test_n_shot(model, dataset, preprocessor, num_tasks, n, k, network_type='siamese'):
    n_correct = 0
    if n == 1 and network_type == 'siamese':
        for _ in tqdm(range(num_tasks)):
            query_sample, support_set_samples = dataset.get_n_shot_samples(k, n)

            input_1 = np.stack([query_sample[0]] * k)[:, :, np.newaxis]
            input_2 = support_set_samples[0][:, :, np.newaxis]
            ([input_1, input_2], _) = preprocessor(([input_1, input_2], []))
            prediction = model.predict([input_1, input_2])
            if np.argmin(prediction[:, 0]) == 0:
                n_correct += 1

    elif n > 1 or network_type == 'classifier':
        # Create encoder network from earlier layers
        if network_type == 'siamese':
            encoder = model.layers[2]
        elif network_type == 'classifier':
            encoder = clone_model(model)
            encoder.set_weights(model.get_weights())
            encoder.pop()

        for _ in tqdm((range(num_tasks))):
            query_sample, support_set_samples = dataset.get_n_shot_samples(k, n)
            preprocessed_query_sample = preprocessor(query_sample[0].reshape(1, -1, 1))
            preprocessed_support_set = preprocessor(support_set_samples[0][:, :, np.newaxis])
            query_embedding = encoder.predict(preprocessed_query_sample)
            support_set_embeddings = encoder.predict(preprocessed_support_set)
            prediction = euclidian_distance(query_embedding, support_set_embeddings, n, k)
            if np.argmin(prediction) == 0:
                n_correct += 1

    return n_correct


def euclidian_distance(query_embedding, support_set_embeddings, n, k):
    # Mean of support set embeddings
    mean_support_set_embeddings = []
    for i in range(0, n * k, n):
        mean_support_set_embeddings.append(support_set_embeddings[i:i + n, :].mean(axis=0))
    mean_support_set_embeddings = np.stack(mean_support_set_embeddings)
    # Euclidean distances between mean embeddings
    prediction = np.sqrt(np.power((np.concatenate([query_embedding] * k) - mean_support_set_embeddings), 2).sum(axis=1))
    return prediction
